const http = require("http");
const express = require("express");
const app = express();
const bodyparser = require("body-parser")


app.use(express.static(__dirname+ '/public'))
app.set('view engine',"ejs");
app.use(bodyparser.urlencoded({extended:true}));


let datos = [{
    matricula:"2020030280",
    nombre: "ACOSTA ORTEGA JESUS HUMBERTO",
    sexo: "M",
    materias: ["Ingles", "Tecnologias de internet", "Base de datos"]

},
{
    matricula:"2020030281",
    nombre: "ACOSTA VARELA IRVING GUADALUPE",
    sexo: "M",
    materias: ["Ingles", "Tecnologias de internet", "Base de datos"]

},
{
    matricula:"2020030282",
    nombre: "ALMOGAR VAZQUEZ YARLEN DE JESUS",
    sexo: "F",
    materias: ["Ingles", "Tecnologias de internet", "Base de datos"]

},
    {
    matricula:"2020030206",
    nombre: "JUAN CARLOS MORENO LOPEZ",
    sexo: "M",
    materias: ["Ingles", "Tecnologias de internet", "Base de datos"]
    
},
{
    matricula:"2020030210",
    nombre: "RUY JESE LUNA SANDOVAL",
    sexo: "M",
    materias: ["Ingles", "Tecnologias de internet", "Base de datos"]

},
{
    matricula:"2020030236",
    nombre: "ERICK JEANICK LOPEZ GONZALES",
    sexo: "M",
    materias: ["Ingles", "Tecnologias de internet", "Base de datos"]

},

]

app.get('/', (req,res)=>{

//    res.send("<h1>Iniciamos con express</h1>");

    res.render('index',{titulo:"Listado de alumnos ",listado:datos})
 


})

app.get('/tabla',(req,res)=>{

    const valores={
        tabla:req.query.tabla
    }
    res.render('tabla',valores)


})

app.post("/tabla", (req,res)=>{
    const valores={
        tabla:req.body.tabla
    }
    res.render('tabla',valores);

})
app.get('/cotizacion',(req,res)=>{
    const cot={
        valor:req.query.valor,
        pInicial:req.query.pInicial,
        plazos:req.query.plazos
    }

    res.render('cotizacion',cot)
})

app.get('/cotizacion',(req,res)=>{
    const cot={
        valor:req.body.valor,
        pInicial:req.body.pInicial,
        plazos:req.body.plazos
    }

    res.render('cotizacion',cot)
})




// escuchar el servidor por el puerto 3000
app.use((req,res,next)=>{

    res.status(404).sendFile(__dirname+  '/public/error.html');
})
const puerto = 3000;

app.listen(puerto,()=>{
    console.log("iniciando puerto 3000")

});
